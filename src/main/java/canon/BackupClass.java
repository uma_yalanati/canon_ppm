package canon;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.openqa.selenium.By.xpath;

/**
 * Created by uyalanat on 16-12-2016.
 */
public class BackupClass {

    private static WebDriver driver;
    private static WebDriver newDriver;
    private static String locator;
    private static boolean isMatchingTextFound = false;
    public static String poNumber = "", currentDate = "";
    //public static ArrayList<String> downServers = new ArrayList<String>();
    public static List<String> downServers = new ArrayList<String>();
    public static ArrayList<String> arrayList = new ArrayList<String>();

    public static void main(String[] args) throws Exception {
        BackupClass home = new BackupClass();
        //   home.testLocal();
        home.readExcelData();
        home.executeScript();
    }

    private void testLocal() throws Exception {
        File file = new File("C:/Users/uyalanat/Desktop/https%20_canoneuropenv.sharepoint.com_sites_pwa_Tasks.aspx.htm");
        FileInputStream fileInputStream = new FileInputStream(file);
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document xmlDocument = builder.parse(fileInputStream);

        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();
//        xPath.

        System.out.println("*************************");
        String expression = "*[@id='zz11_RootAspMenu']";
        System.out.println(expression);
        String email = xPath.compile(expression).evaluate(xmlDocument);
        System.out.println(email);

        System.out.println("*************************");
    }

    private void savePageSource(String fileName, String source) {
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(source);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executeScript() throws Exception {
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
        launchBrowser("https://canoneuropenv.sharepoint.com/sites/pwa/Tasks.aspx");
        enterValue("id:=cred_userid_inputtext->username->Phaniraj.Agarwal@contractors.canon-europe.com");
        enterValue("id:=cred_password_inputtext->password->Oct_@327");
        click("id:=cred_sign_in_button->Sign In");
        waitTime("3");
        clearTextBox("id:=userNameInput");
        enterValue("id:=userNameInput->username->Phaniraj.Agarwal@contractors.canon-europe.com");
        enterValue("id:=passwordInput->password->Oct_@327");
        click("id:=submitButton->Sign In");
        waitTime("3");
        waitForElement("xpath:=.//*[@id='zz11_RootAspMenu']/li[2]/a->Tasks");
        click("xpath:=.//*[@id='zz11_RootAspMenu']/li[2]/a->Tasks");
        waitForElement("xpath:=.//*[@id='ctl00_ctl00_ctl32_g_94d974b4_8f1a_43f5_ae0d_883bbc6a5d1f_MyTasksJSGridControl_leftpane_mainTable']/tbody/tr[1]/th[4]/div[2]/div[2]/div/a->Resource Name");
        click("xpath:=.//*[@id='ctl00_ctl00_ctl32_g_94d974b4_8f1a_43f5_ae0d_883bbc6a5d1f_MyTasksJSGridControl_leftpane_mainTable']/tbody/tr[1]/th[4]/div[2]/div[2]/div/a->Resource Name");
        waitForElement("xpath:=.//*[@id='Ribbon.ContextualTabs.MyWork.Home.Period.SelectPeriod-Large']/span[2]->Select Period");
        click("xpath:=.//*[@id='Ribbon.ContextualTabs.MyWork.Home.Period.SelectPeriod-Large']/span[2]->Select Period");
        waitTime("5");
        // switchToFrame(".//*[@id='ctl00_ctl00_PlaceHolderMain_PWA_PlaceHolderMain_mnuPeriods']", currentDate, true);
        switchToFrame("ctl00_ctl00_PlaceHolderMain_PWA_PlaceHolderMain_mnuPeriods", currentDate, true);
        //  handleWindow();
        waitTime("10");
        click("id:=Ribbon.ContextualTabs.MyWork.Home.Data.FilterDropdown->Filter");
        click("xpath:=.//*[@id='Ribbon.ContextualTabs.MyWork.Home.Data.FilterDropdown.Menu.InboxCustomFilter.Custom-Menu16']/span[3]->Custom Filter...");
        waitTime("6");
        switchToFrame("ctl00_ctl00_PlaceHolderMain_PWA_PlaceHolderMain_idSelFieldName_0", "PO", false);
        waitTime("3");
        switchToFrame("ctl00_ctl00_PlaceHolderMain_PWA_PlaceHolderMain_idSelOperator_0", "equals", false);


        waitTime("6");
        clearTextBox("id:=ctl00_ctl00_PlaceHolderMain_PWA_PlaceHolderMain_idTxtText_0");
        enterValue("id:=ctl00_ctl00_PlaceHolderMain_PWA_PlaceHolderMain_idTxtText_0->Value->" + poNumber);
        click("id:=ctl00_ctl00_PaceHolderMain_idOkButton->OK");
        if (driver != null) {
            driver.switchTo().parentFrame();
            readHomeData();
        }

    }

    public List<WebElement> getFrames() {
        return driver.findElements(By.xpath("(//frame | //iframe)"));
    }

    public boolean searchFramesForElement(By locator) {
        boolean elementFound = false;
        System.out.println("List of frames " + getFrames().size());
        for (WebElement frame : getFrames()) {
            newDriver = driver.switchTo().defaultContent();
            newDriver = driver.switchTo().frame(frame);
            if (newDriver.findElements(locator).size() > 0) {
                elementFound = true;
                driver.switchTo().frame(0);
                break;
            }
        }
        System.out.println("Home.searchFramesForElement, elementFound: " + elementFound);
        return elementFound;
    }

    private void switchToFrame(String param, String currentelement, Boolean isBoolean) throws Exception {

        BackupClass home = new BackupClass();
        if (home.searchFramesForElement(By.id(param))) {
            //Select sel = new Select(driver.findElement(By.xpath("//select[@name='"+currentDate+"']")));
            Select selectBox = new Select(driver.findElement(By.id(param)));
            selectBox.selectByVisibleText(currentelement);
            if (isBoolean) {
                click("id:=ctl00_ctl00_PlaceHolderMain_idOkButton->Ok");
            }
            driver.switchTo().defaultContent();
        } else {
            System.out.println("uma");
        }

    }

    private void handleWindow() throws InterruptedException {


//handle of the master window before clicking the link
        String master = driver.getWindowHandle();

        // driver.findElement(By.linkText("Click me")).click();

//logic for waiting for the popup, checking the size to become greater than 1 or breaking after sometime to avoid the infinite loop.
        int timeCount = 1;

        do {
            driver.getWindowHandles();
            Thread.sleep(200);
            timeCount++;
            if (timeCount > 50) {
                break;
            }
        }
        while (driver.getWindowHandles().size() == 1);

//Assigning the handles to a set
        Set<String> handles = driver.getWindowHandles();
//Switching to the popup window.
        for (String handle : handles) {
            if (!handle.equals(master)) {
                driver.switchTo().window(handle);
                Select selectBox = new Select(driver.findElement(By.xpath(".//*[@id='ctl00_ctl00_PlaceHolderMain_PWA_PlaceHolderMain_mnuPeriods']")));
                selectBox.selectByVisibleText(currentDate);
                click("id:=ctl00_ctl00_PlaceHolderMain_idOkButton->Ok");
            }
        }


    }

    public void switchToFrameByIndex(int frame) {
        try {
            driver.switchTo().frame(frame);
            System.out.println("Navigated to frame with id " + frame);
        } catch (NoSuchFrameException e) {
            System.out.println("Unable to locate frame with id " + frame
                    + e.getStackTrace());
        } catch (Exception e) {
            System.out.println("Unable to navigate to frame with id " + frame
                    + e.getStackTrace());
        }
    }

    private static void launchBrowser(String url) throws IOException {
        FirefoxProfile profile = new FirefoxProfile();
        driver = new FirefoxDriver(profile);
        driver.get(url);
        driver.manage().window().maximize();
        System.out.println("Sucessfully launched application");
    }

    private static WebElement getLocator(String parameters) {


        WebElement locpath = null;
        try {

            String[] arguments = null;
            boolean b;
            b = parameters.matches(".*:.*");

            if (b) {
                arguments = splitfunction(parameters, ":=");
            } else {
                arguments = splitfunction(parameters, "\\|");
            }


            String mode = arguments[0];
            arguments[0].trim();
            locator = arguments[1];
            arguments[1].trim();

            if (mode.equals("id")) {
                locpath = driver.findElement(By.id(locator));

            } else if (mode.equals("name")) {
                locpath = driver.findElement(By.name(locator));

            } else if (mode.equals("linkText")) {
                locpath = driver.findElement(By.linkText(locator));

            } else if (mode.equals("xpath")) {
                locpath = driver.findElement(xpath(locator));

            } else if (mode.equals("cssSelector")) {
                locpath = driver.findElement(By.cssSelector(locator));

            } else if (mode.equals("partialLinkText")) {
                locpath = driver.findElement(By.partialLinkText(locator));

            } else if (mode.equals("className")) {
                locpath = driver.findElement(By.className(locator));

            } else if (mode.equals("tagName")) {
                locpath = driver.findElement(By.tagName(locator));

            }

        } catch (Exception e) {

            //e.printStackTrace();
            System.out.println("unable to find the locator" + " " + locator);

        }
        return locpath;
    }

    private static String[] splitfunction(String keyparameters, String symbol) {

        String[] parameters = keyparameters.split(symbol);
        return parameters;
    }

    private static void enterValue(String parameters) {
        String[] arguments;
        arguments = splitfunction(parameters, "\\->");
        try {
            WebElement locator = getLocator(arguments[0]);
            if (locator.isEnabled()) {
                String value = arguments[2];
                locator.sendKeys(value);
                System.out.println("Succesfully entered the value" + " " + arguments[2]);
            } else {
                System.out.println("not able to find" + " " + arguments[1]);
            }
        } catch (Exception e) {

            //e.printStackTrace();
            System.out.println("unable to find the locator" + " " + locator + "" + arguments[1]);
        }

    }

    private static void waitTime(String parameters) {

        try {
            int sleepTime = Integer.valueOf(parameters) * 1000;
            System.out.println("sleepTime: " + sleepTime);
            Thread.sleep(sleepTime);
            System.out.println("waited " + sleepTime);

        } catch (Exception e) {
            System.out.println("exception value : " + e.getMessage());

        }

    }

    private static void click(String parameters) throws InterruptedException {
        Thread.sleep(3000);
        String[] arguments;
        arguments = splitfunction(parameters, "->");
        try {
            WebElement locator = getLocator(arguments[0]);
            if (locator.isEnabled()) {
                locator.click();
                System.out.println("Succesfully clicked on" + " " + arguments[1]);
            } else {
                System.out.println("unable to find" + " " + arguments[1]);
            }
        } catch (Exception e) {

            System.out.println("unable to find the locator" + " " + arguments[1]);

        }

    }

    private static void waitForElement(String parameters) {
        boolean status = true;
        try {
            int flag = 1;
            String[] arguments;
            arguments = splitfunction(parameters, "->");
            int timer = 6000;
            do {
                WebElement locator = getLocator(arguments[0]);
                if (locator == null) {
                    timer = timer - 1;
                    if (timer == 0) {
                        timer = 6001;
                    }
                } else {
                    timer = 6001;
                    flag = 0;
                    System.out.println("Element found" + " " + arguments[1]);
                }
            } while (timer < 6000);

            if (flag == 1) {
                System.out.println("element not found " + arguments[1]);
            }

        } catch (TimeoutException e) {
            System.out.println("did not found the element");
        }

    }

    private static void switchWindow(String parameters) throws InterruptedException {
        String exptitle;
        // boolean status ;
        Thread.sleep(4000);
        int flag = 0;
        try {
            String[] arguments;
            arguments = splitfunction(parameters, "->");
            exptitle = arguments[1];
            Set<String> allWindowHandles = driver.getWindowHandles();

            for (String handle : allWindowHandles) {
                driver.switchTo().window(handle);
                String acttitle = driver.getTitle();
                if (acttitle.equalsIgnoreCase(exptitle)) {
                    driver.switchTo().window(handle);
                    System.out.println("moved to child window" + " " + exptitle);
                    flag = 1;
                    break;
                }
            }
            if (flag == 1) {
                // status = true;
            } else {
                System.out.println("unable to find window" + " " + exptitle);
            }

        } catch (Exception e) {
            System.out.println("unable to find  child  window ");
        }


    }

    private static void selectValueFromDropdown(String parameters) throws InterruptedException {
        Thread.sleep(3000);
        String[] arguments;
        boolean checkstatus = false;
        arguments = splitfunction(parameters, "->");
        isMatchingTextFound = false;
        try {
            WebElement locator = getLocator(arguments[0]);
            Select oSelect = new Select(locator);
            List<WebElement> elementCount = oSelect.getOptions();
            int iSize = elementCount.size();
            for (int i = 0; i < iSize; i++) {
                String sValue = elementCount.get(i).getText();
                if (sValue.equalsIgnoreCase(arguments[2])) {
                    checkstatus = true;
                    if (ISNUMERIC(arguments[2])) {
                        if (arguments[2].length() == 1) {
                            int oindex = Integer.parseInt(arguments[2]);
                            oSelect.selectByIndex(oindex);
                            System.out.println("Selected the element with index " + " " + arguments[2]);
                        } else {
                            oSelect.selectByValue(arguments[2]);
                            System.out.println("Selected the value " + " " + arguments[2]);
                        }
                    } else {
                        isMatchingTextFound = true;
                        oSelect.selectByVisibleText(arguments[2]);
                        System.out.println("Selected the text" + " " + arguments[2]);
                    }
                    break;
                }

            }
            if (checkstatus != true) {
                System.out.println("unable to find the element" + " " + arguments[2]);
            }

        } catch (Exception e) {
            System.out.println("unable to find the element" + " " + arguments[2]);
        }


    }

    private static boolean ISNUMERIC(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static void readHomeData() {
        WebElement table_element_right = driver.findElement(By.id("ctl00_ctl00_ctl32_g_94d974b4_8f1a_43f5_ae0d_883bbc6a5d1f_MyTasksJSGridControl_rightpane_mainTable"));
        List<WebElement> tr_collection_right = table_element_right.findElements(By.tagName("tr"));
        System.out.println("RIGHT SIDE NUMBER OF ROWS IN THIS TABLE = " + tr_collection_right.size());
//        WebElement table_element = driver.findElement(By.id("ctl00_ctl00_ctl32_g_94d974b4_8f1a_43f5_ae0d_883bbc6a5d1f_MyTasksJSGridControl_leftpane_mainTable"));
        WebElement table_element = driver.findElement(By.id("ctl00_ctl00_ctl32_g_94d974b4_8f1a_43f5_ae0d_883bbc6a5d1f_MyTasksJSGridControl_leftpane_mainTable"));
        List<WebElement> tr_collection = table_element.findElements(By.tagName("tr"));

        System.out.println("NUMBER OF ROWS IN THIS TABLE = " + tr_collection.size());
        int row_num, col_num;
        row_num = 1;
       /* WebElement myElement = driver.findElement(By.id("username"));
        myElement.sendKeys("text");*/
        String initialValue = "";
        for (WebElement trElement : tr_collection) {
            List<WebElement> td_collection = trElement.findElements(xpath("td"));
            System.out.println("NUMBER OF COLUMNS=" + td_collection.size());
            col_num = 1;

            for (WebElement tdElement : td_collection) {
                System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
                System.out.println(tdElement.getAttribute("id"));
                if (row_num == 2 && col_num == 5) {
                    if (tdElement.getText().equals("Planning Window: In Progress for Current Period")) {
                        initialValue = tdElement.getText();
                    } else if (tdElement.getText().equals("Cross_BI")) {
                        initialValue = tdElement.getText();
                    }
                   /* if (initialValue.equals("")) {
                        downServers.add(tdElement.getText());
                        break;
                    }*/

                }
                if (col_num == 4) {
                    downServers.add(tdElement.getText());
                }
                col_num++;
            }
            row_num++;
        }
        driver.switchTo().frame(1);
        for (int y = 0; y < downServers.size(); y++) {
            // if (downServers.get(y).equals(""))
            System.out.println("downServers------------------" + downServers.get(y));
        }
        clearTextBox("id:=ctl00_ctl00_ctl32_g_94d974b4_8f1a_43f5_ae0d_883bbc6a5d1f_MyTasksJSGridControl_1_2_1");
        enterValue("id:=ctl00_ctl00_ctl32_g_94d974b4_8f1a_43f5_ae0d_883bbc6a5d1f_MyTasksJSGridControl_1_2_1->1");
        //   driver.switchTo().frame(1);


        //    driver.findElement(By.name("q")).sendKeys(A);
        //System.out.println(downServers);
    }


    private static void clearTextBox(String parameters) {
        try {
            WebElement locator = getLocator(parameters);
            if (locator.isEnabled()) {
                locator.clear();
                System.out.println("Succesfully cleared the text box");
                //ReportFunctions.LogRepoter("pass", "clear the text box", "Succesfully cleared the text box");
            } else {
                System.out.println("Text box was disabled");
                //ReportFunctions.LogRepoter("Fail", "clear the  text box", "Text box was disabled");
            }
        } catch (Exception e) {

            //e.printStackTrace();
            System.out.println("unable to find the locator" + " " + locator);
            //ReportFunctions.LogRepoter("Fail", "clear the  text box", "unable to find the locator" + " " +arguments[1]);
        }


    }


    public void readExcelData() {
        String rowval = "";
        try {

            File src = new File("C:\\Users\\uyalanat\\Downloads/PPM.xlsx");

            FileInputStream fis = new FileInputStream(src);

            XSSFWorkbook wb = new XSSFWorkbook(fis);

            XSSFSheet sh1 = wb.getSheetAt(0);


            System.out.println(sh1.getRow(0).getCell(0).getStringCellValue());
            System.out.println(sh1.getRow(0).getCell(1).getStringCellValue());
            System.out.println(sh1.getRow(0).getCell(2).getStringCellValue());

            System.out.println(sh1.getRow(0).getCell(3).getStringCellValue());
            System.out.println(sh1.getRow(0).getCell(4).getStringCellValue());
            System.out.println(sh1.getRow(0).getCell(5).getStringCellValue());
            System.out.println(sh1.getRow(0).getCell(6).getStringCellValue());
            System.out.println(sh1.getRow(0).getCell(7).getStringCellValue());
            System.out.println(sh1.getRow(0).getCell(8).getStringCellValue());


            System.out.println(sh1.getRow(1).getCell(0).getStringCellValue());

            DecimalFormat df = new DecimalFormat("###.#");
            System.out.println(df.format(sh1.getRow(1).getCell(1).getNumericCellValue()));
            //  System.out.println(sh1.getRow(1).getCell(1).getNumericCellValue());
            poNumber = String.valueOf(df.format(sh1.getRow(1).getCell(1).getNumericCellValue()));
            System.out.println(sh1.getRow(1).getCell(2).getStringCellValue());
            currentDate = sh1.getRow(1).getCell(2).getStringCellValue();
            System.out.println(sh1.getRow(2).getCell(0).getStringCellValue());

            System.out.println(sh1.getRow(2).getCell(1).getNumericCellValue());

            for (int i = 1; i < 50; i++) {
                arrayList.add(sh1.getRow(i).getCell(3).getStringCellValue());
                arrayList.add(sh1.getRow(i).getCell(4).getStringCellValue());
                arrayList.add(sh1.getRow(i).getCell(5).getStringCellValue());
                arrayList.add(sh1.getRow(i).getCell(6).getStringCellValue());
                arrayList.add(sh1.getRow(i).getCell(7).getStringCellValue());
                arrayList.add(sh1.getRow(i).getCell(8).getStringCellValue());


            }
        } catch (Exception e) {

            System.out.println(e.getMessage());

        }

    }

}
