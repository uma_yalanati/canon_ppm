package canon;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Properties;

/**
 * Created by uyalanat on 04-04-2017.
 */
public class Lumentum {

    public static WebDriver driver;

    //public static String driverPath="C:\\Program Files (x86)\\Internet Explorer\\IEDriverServer.exe";
//    public static String driverPath = "C:\\Users\\uyalanat\\Downloads\\IEDriverServer.exe";
//    public static String driverPath = "C:\\Users\\dsasidha\\Downloads\\IEDriverServer_x64_3.3.0";

    public static void main(String[] args) {
        String driverLocation = null;
        File file = new File("lum.properties");
        FileInputStream fis= null;
        try {
             fis = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fis);

            driverLocation = properties.getProperty("driver.location");
        } catch (FileNotFoundException e) {
            System.out.println("lum.properties is missing...");
            System.out.println("Expected file path: "+file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if(driverLocation!=null){
            System.out.println("Loading drier from Location: "+driverLocation);

            try {
                run(driverLocation);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("Web driver location is not available");
        }
//        System.out.println(file.getAbsolutePath());
    }

    public static void run(String driverPath) throws InterruptedException, MalformedURLException {

        System.out.println("*******************");
        System.out.println("launching IE browser");
//        System.setProperty("webdriver.ie.driver", "C:\\Users\\uyalanat\\Downloads\\"+"IEDriverServer.exe");
        System.setProperty("webdriver.ie.driver", driverPath);
        driver = new InternetExplorerDriver();
        driver.manage().window().maximize();

        launchBrowser();

        driver.get("https://epmprod.dc.lumentuminc.net/workspace/index.jsp");
//        WebElement useName=driver.findElement(By.id("bpm.Logon.1.lblUser"));

        //  waitForElement("bpm.Logon.1.txtUser", driver);

        WebDriverWait webDriverWait = new WebDriverWait(driver, 5000);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bpm.Logon.1.txtUser")));

        WebElement useName = driver.findElement(By.id("bpm.Logon.1.txtUser"));
        //WebElement usePassword=driver.findElement(By.id("bpm.Logon.1.txtPassword"));


        Actions builder = new Actions(driver);

        //Clear UserName
        useName.clear();

        //Enter userName
        Actions seriesofActions = builder.moveToElement(useName).click().sendKeys(useName, "CorporateFPA");
        seriesofActions.perform();


        //Entering Password
        WebElement usePassword = driver.findElement(By.id("bpm.Logon.1.lblPassword"));
        WebElement login = driver.findElement(By.id("bpm.Logon.1.btnLogon"));
        Actions seriesofActionsPassword = builder.moveToElement(useName).click().sendKeys(usePassword, "password").click(login);
        seriesofActionsPassword.perform();

        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wksp.mode.tlbt_ExploreMode")));
        WebElement explorer = driver.findElement(By.id("wksp.mode.tlbt_ExploreMode"));
        Actions seriesofActionsExplore = builder.click(explorer);
        seriesofActionsExplore.perform();



        Thread.sleep(5000);

        List<WebElement> development = driver.findElements(By.xpath("//*[contains(text(), 'Development')]"));
/*
        for (int i=0;i<development.size();i++){

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(development.get(i)));
            System.out.println(development.get(i));

            if (development.get(i).getText().equals("Development")) {
                Actions series = builder.doubleClick(development.get(i));
                series.perform();
                break;
            }
        }*/

        for (WebElement webElement : development) {
            //  webDriverWait.until(webElement);
            //  webDriverWait.until(webElement);
            Thread.sleep(5000);
            System.out.println(webElement.getText());
            if (webElement.getText().equals("Development")) {
                Actions series = builder.doubleClick(webElement);
                series.perform();
                break;
            }
        }

        Thread.sleep(10000);
       /* List<WebElement> estaff = driver.findElements(By.xpath("/*//*[contains(text(), 'E-Staff')]"));

        for (WebElement webElement : estaff) {
            Thread.sleep(5000);
            if (webElement.getText().equals("E-Staff")) {
                Actions series = builder.doubleClick(webElement);
                series.perform();
                break;
            }
        }*/

        List<WebElement> estaff = driver.findElements(By.xpath("//*[contains(text(), 'Lumentum')]"));

        for (WebElement webElement : estaff) {
            Thread.sleep(6000);
            if (webElement.getText().equals("Lumentum Reports New")) {
                Actions series = builder.doubleClick(webElement);
                series.perform();
                break;

            }

        }

        Thread.sleep(8000);
        List<WebElement> estaff_pl = driver.findElements(By.xpath("//*[contains(text(), 'E-staff')]"));

        for (WebElement webElement : estaff_pl) {
            //   Thread.sleep(5000);
            if (webElement.getText().equals("E-staff P&L- BETA")) {
                Actions series = builder.doubleClick(webElement);
                series.perform();
                break;
            }
        }

        Thread.sleep(10000);
       // List<WebElement> estaff_ile = driver.findElements(By.xpath("/[contains(text(), 'ile')]"));

        List<WebElement> estaff_ile = driver.findElements(By.xpath("//*[contains(text(), 'ile')]"));

        for (WebElement webElementile : estaff_ile) {
            //   Thread.sleep(5000);
            if (webElementile.getText().equals("File")) {
                Actions series = builder.click(webElementile).sendKeys("Export").sendKeys("Save");
                series.perform();
                break;
            }
        }
        Thread.sleep(8000);



        try {
            Robot robot=new Robot();

            robot.keyPress(KeyEvent.VK_DOWN);
            Thread.sleep(5000);
            // robot.keyPress(KeyEvent.VK_TAB);
            //  Thread.sleep(1000);
            robot.keyPress(KeyEvent.VK_ENTER);
        } catch (AWTException e) {
            e.printStackTrace();
        }

        Thread.sleep(5000);

        driver.quit();

        try {
          // Runtime.getRuntime().exec("taskkill /F /IM IEDriver.exe");

            Runtime.getRuntime().exec("taskkill /F /IM iexplore.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void launchBrowser() {

        driver.navigate().to("https://epmprod.dc.lumentuminc.net/workspace/index.jsp");
        String strPageTitle = driver.getTitle();
        System.out.println("Page title: - " + strPageTitle);
    }
}
