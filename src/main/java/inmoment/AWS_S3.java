package inmoment;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;


public class AWS_S3
{
  public static String AWS_REGION_EAST_1;
  public static String AWS_ACCESS_KEY_ID;
  public static String AWS_SECRET_KEY_ID;
  
  public AWS_S3() {}
  
  public void deleteFile(String bucketName, String bucketKey)
  {
    AmazonS3 s3Client = getS3Client();
    s3Client.deleteObject(bucketName, bucketKey);
  }
  

  public void uploadFile(String bucketName, String bucketKey, String filePath)
  {
 //   Inmoment inm = new Inmoment();
    AWS_REGION_EAST_1 = Inmoment.AWS_REGION_EAST_1;
    AWS_ACCESS_KEY_ID = Inmoment.AWS_ACCESS_KEY_ID;
    AWS_SECRET_KEY_ID = Inmoment.AWS_SECRET_KEY_ID;
    System.out.println(AWS_REGION_EAST_1 + " " + AWS_ACCESS_KEY_ID + " " + AWS_SECRET_KEY_ID);
    File file = new File(filePath);
  //  byte[] fileContent = new byte[(int)file.length()];
    
    System.out.println("Uploading...");
    System.out.println("bucketName----"+bucketName);
    System.out.println("bucketKey----"+bucketKey);
    AmazonS3 s3Client = getS3Client();
    
    File f = new File(filePath);
    System.out.println(f.getName());
    s3Client.putObject(new PutObjectRequest(bucketName, bucketKey, new File(filePath)));
    

    System.out.println("Successfully Uploaded....");
  }
  
//, 
  public AmazonS3 getS3Client() {
	    return AmazonS3ClientBuilder.standard()
	            .withRegion(Regions.fromName(AWS_REGION_EAST_1))
	            .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY_ID)))
	            .build();
	}
}
