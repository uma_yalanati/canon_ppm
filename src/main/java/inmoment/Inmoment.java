package inmoment;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Inmoment {
    static WebDriver driver = null;
    static WebDriverWait wait;
    static Properties prop = new Properties();
    public static String browser_path;
    public static String username;
    public static String password;

    public Inmoment() {
    }

    public static void setup() {
        String browser_path = CURR_WORKING_DIR + "/chromedriver.exe";
        System.out.println(browser_path);
        System.setProperty("webdriver.chrome.driver", browser_path);

        String downloadFilepath = CURR_WORKING_DIR + "/Results";

        HashMap<String, Object> chromePrefs = new HashMap();
        chromePrefs.put("profile.default_content_settings.popups", Integer.valueOf(0));
        chromePrefs.put("download.default_directory", downloadFilepath);
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromeOptionsMap = new HashMap();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments(new String[]{"--test-type"});
        options.addArguments(new String[]{"--disable-extensions"});

        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability("goog:chromeOptions", chromeOptionsMap);
        cap.setCapability("acceptSslCerts", true);
        cap.setCapability("goog:chromeOptions", options);
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

        driver = new org.openqa.selenium.chrome.ChromeDriver(cap);
        driver.manage().window().maximize();
     //   driver.navigate().to("https://www.inmoment.com/focus");
        driver.navigate().to("https://www.inmoment.com/focus/#/units/880041/views/30064772066?page=LegacyReports&reportId=140750");
//25Mar2019
        driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);
        System.out.println("Sucessfully browser launch");
    }

    public static String CURR_WORKING_DIR;
    public static String startdate;
    public static String enddate;
    public static String reportformat;

    public static void Login() {
        driver.findElement(By.id("username")).sendKeys(new CharSequence[]{username});
        driver.findElement(By.id("password")).sendKeys(new CharSequence[]{password});
        driver.findElement(By.id("Any_3")).click();
        System.out.println("Successfully launched Announcement Page");
    }

    public static void Home() {
      //  driver.findElement(By.xpath("//*[@id='navMenu']/ul/li[4]/a")).click();
        driver.findElement(By.partialLinkText("Reporting")).click();
        System.out.println("Sucessfully launched Reporting Page");
    }

    public static void Reports() {
        wait = new WebDriverWait(driver, 30L);
       /* try {
            Thread.sleep(4000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        ((WebElement) wait.until(org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='content']/div/div/div[3]/div[1]/div[2]/div[1]/div/ul/li[1]/div/span")))).click();
        System.out.println("Successfully Clicked on Brand");

        driver.findElement(By.xpath("//*[@id='content']/div/div/div[3]/div[1]/div[2]/div[1]/div/ul/li[1]/div/ul/li[29]/span")).click();
        System.out.println("Successfully Clicked on Deleted Response IDS");
    }

    public static void Date_Dropdown() throws InterruptedException {
        wait = new WebDriverWait(driver, 30L);


        WebElement frame1 = driver.findElement(By.xpath("//*[@ng-if='!isMobileApp && selectedReport']"));
        driver.switchTo().frame(frame1);
        System.out.println("swithced to frame1");

        if ((startdate.isEmpty()) && (enddate.isEmpty())) {

            driver.findElement(By.id("field")).clear();
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            System.out.println("Today's date is " + dateFormat.format(cal.getTime()));
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);
            cal.add(5, -90);
            driver.findElement(By.id("field")).sendKeys(new CharSequence[]{dateFormat.format(cal.getTime())});

            driver.findElement(By.id("field_0")).clear();
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);
            Calendar cala = Calendar.getInstance();
            SimpleDateFormat dateFormate = new SimpleDateFormat("MM/dd/yyyy");
            System.out.println("Today's date is " + dateFormate.format(cala.getTime()));
            cala.add(5, -1);
            driver.findElement(By.id("field_0")).sendKeys(new CharSequence[]{dateFormate.format(cala.getTime())});
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);

        } else if (enddate.isEmpty()) {

            driver.findElement(By.id("field")).clear();
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);
            driver.findElement(By.id("field")).sendKeys(new CharSequence[]{startdate});


            driver.findElement(By.id("field_0")).clear();
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);
            Calendar cala = Calendar.getInstance();
            SimpleDateFormat dateFormate = new SimpleDateFormat("MM/dd/yyyy");
            System.out.println("Today's date is " + dateFormate.format(cala.getTime()));
            cala.add(5, -1);
            driver.findElement(By.id("field_0")).sendKeys(new CharSequence[]{dateFormate.format(cala.getTime())});
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);

        } else {

            driver.findElement(By.id("field")).clear();
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);
            driver.findElement(By.id("field")).sendKeys(new CharSequence[]{startdate});

            driver.findElement(By.id("field_0")).clear();
            driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);
            driver.findElement(By.id("field_0")).sendKeys(new CharSequence[]{enddate});
        }


        Select format = new Select(driver.findElement(By.id("format")));
        format.selectByVisibleText(reportformat);
        driver.manage().timeouts().implicitlyWait(180L, TimeUnit.SECONDS);

        driver.findElement(By.id("runButton")).click();
        Thread.sleep(60000L);
        wait = new WebDriverWait(driver, 30L);
        System.out.println("dowloading ...");
    }

    public static String bucketName;
    public static String bucketKey;


    private static CharSequence getDate(Calendar cal) {
        return null;
    }

    public static String AWS_ACCESS_KEY_ID;
    public static String AWS_SECRET_KEY_ID;
    public static String AWS_REGION_EAST_1;

    public static void getproperties() throws IOException {
        CURR_WORKING_DIR = new File(".").getCanonicalPath();
        try {
            java.io.InputStream input = new FileInputStream(CURR_WORKING_DIR + "/config.properties");

            if (input != null) {
                prop.load(input);
            }
            browser_path = prop.getProperty("browser_path");
            username = prop.getProperty("username");
            password = prop.getProperty("password");
            startdate = prop.getProperty("startdate");
            enddate = prop.getProperty("enddate");
            reportformat = prop.getProperty("reportformat");
            bucketName = prop.getProperty("bucketName");
            bucketKey = prop.getProperty("bucketKey");
            AWS_REGION_EAST_1 = prop.getProperty("AWS_REGION_EAST_1");
            AWS_ACCESS_KEY_ID = prop.getProperty("AWS_ACCESS_KEY_ID");
            AWS_SECRET_KEY_ID = prop.getProperty("AWS_SECRET_KEY_ID");
            System.out.println(browser_path + " " + username + " " + password + " " + startdate + " " + enddate + " " + reportformat + " " +
                    bucketName + " " + bucketKey + " " + AWS_REGION_EAST_1 + " " + AWS_ACCESS_KEY_ID + " " + AWS_SECRET_KEY_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stopBrowser() {
        driver.quit();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        getproperties();


        File root = new File(CURR_WORKING_DIR + "/Results");
        File[] Files = root.listFiles();
        if (Files != null) {
            for (int j = 0; j < Files.length; j++) {//
                System.out.println(Files[j].getAbsolutePath());
                System.out.println(Files[j].delete());

            }


        } else {

            System.out.println("Report not available");
        }


        setup();
        Login();
       // Home();
       // Reports();
        Date_Dropdown();
        stopBrowser();


        AWS_S3 UploadFileDao = new AWS_S3();
     //   String filePath = CURR_WORKING_DIR + "/Results/" + "Deleted Response IDs.xls";
        File file = new File(CURR_WORKING_DIR + "/Results");
        File[] files = file.listFiles();
        String fileName="Deleted Response IDs.xls";
        for (File f : files) {
            System.out.println("FileNames----" + f.getName());
            fileName=f.getName();
        }
        String filePath = CURR_WORKING_DIR + "/Results/" + fileName;
        UploadFileDao.uploadFile(bucketName, bucketKey, filePath);
    }
    //Returns webelement
    public static WebElement expandRootElement(WebElement element) {
        WebElement ele = (WebElement) ((JavascriptExecutor) driver)
                .executeScript("return arguments[0].shadowRoot",element);
        return ele;
    }

}
